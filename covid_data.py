from itertools import count
import urllib.request 
import json 
import csv
import datetime as dt
from datetime import datetime
import pandas as pd
import psycopg2
import sqlite3

class DatabaseConnect:
    
    # Constructor handles the credentials, connection and cursor creation
    def __init__(self):
        
        print("Attempting to connect to the PostgreSQL database server... ")
        
        self.connect = psycopg2.connect(   
                host="localhost",
                database="covid_data",
                user="postgres",
                password="Password",
                port= '5432')
        
        
        self.connect.autocommit = True
        self.cursor = self.connect.cursor()
        
        print('Connection established\nPostgreSQL database version:')
        self.cursor.execute('SELECT version()')
        
        db_version = self.cursor.fetchone()
        print(db_version,"\n")
    
    # Closing the connection and cursor after script finishes
    def close_connection(self) -> None:
        print("Closing the connection...")
        self.cursor.close()
        self.connect.close()
    
    # Converts the last 14 days from today into appropriate format
    @staticmethod
    def convert_today_to_year_week() -> tuple[str, ...]:
        my_date = dt.date.today() 
        year, week_number, day_of_week = my_date.isocalendar()
        dates = [f"{year}-{week_number - i}" for i in range(1, 3)]
        return tuple(dates)

    # Converts a date into appropriate format
    @staticmethod
    def convert_to_year_week(date: str) -> str:
        date_object = datetime.strptime(date, '%d/%m/%Y')
        year, week_number, day_of_week = date_object.isocalendar()
        result = f"{year}-{week_number}"
        return result
    
    # Returns a list of sorted tuples based on the second element of each tuple
    @staticmethod
    def sort_fetch(fetch) -> list:
        return sorted(fetch, key=lambda item: item[1], reverse=True)

        
class CovidData(DatabaseConnect):
    def __init__(self):
        super().__init__()
        
    # Fetching the JSON file as 'url' object and then loading it into 'covid_data'
    def fetch_json(self):
        with urllib.request.urlopen("https://opendata.ecdc.europa.eu/covid19/nationalcasedeath/json") as url:
            covid_data = json.load(url)
        print("\tJSON with the Covid-19 data has been fetched")
        return covid_data
    
    # Creates the covid_data table in PostgreSQL
    def create_covid_table(self) -> None:
        query = """
                CREATE TABLE IF NOT EXISTS covid_data (
                country TEXT,
                country_code TEXT,
                continent TEXT,
                population INT,
                indicator TEXT,
                weekly_count INT,
                year_week TEXT,
                rate_14_day FLOAT,
                cumulative_count INT,
                source TEXT,
                note TEXT
                )
                """
        self.cursor.execute(query)
        print("\tTable 'covid_data' has been successfully created or already exists")
    
    # Method makes uses an SQL query to compare columns and populates/updates the table accordingly
    def run_data_pipeline(self, data: list) -> None:
        query = """
                INSERT INTO covid_data
                SELECT * FROM json_populate_recordset(NULL::covid_data, %s)
                WHERE(year_week, weekly_count)
                NOT IN (SELECT year_week, weekly_count FROM covid_data)
                """
        self.cursor.execute(query, (json.dumps(data),))
        print("\tTable 'covid_data' has been successfully populated/updated")
    
    # View with the latest number of cases spanning past 2 weeks
    def create_view_cases_last_fortnight(self) -> None:
        week1, week2 = self.convert_today_to_year_week()
        query = """
                CREATE OR REPLACE VIEW cases_last_fortnight AS
                SELECT continent, country, 
                       round(CAST((100 / NULLIF(cumulative_count, 0) * 100000) AS NUMERIC), 2)
                       "cumulative_number_for_14_days_of_COVID-19_cases_per_100000",  
                       indicator, year_week
                FROM covid_data
                WHERE indicator = 'cases' AND cumulative_count IS NOT NULL
                AND (year_week = %s OR year_week = %s)
                """
        self.cursor.execute(query, (week1, week2))
        print("\tView 'cases_last_fortnight' has been created or replaced.\n\tResults are from", week1, "and", week2)
        
        query = """SELECT * FROM cases_last_fortnight"""
        self.cursor.execute(query)
        view = self.cursor.fetchall()
        for continent, country, cumulative_count, indicator, year_week in view:
            print(f"\t{continent} {country} {cumulative_count} {indicator} {year_week}")
        
    # Get country with the highest number of Covid-19 cases per 100 000 inhabitants
    def get_country_with_highest_covid_cases(self, date: str) -> str:
        date = self.convert_to_year_week(date)
        query = """
                SELECT country, (MAX(100 / NULLIF(cumulative_count, 0) * 100000))::float
                FROM covid_data
                WHERE year_week = %s AND cumulative_count > 0
                GROUP BY cumulative_count, country
                ORDER BY cumulative_count DESC
                """
        self.cursor.execute(query, (date,))
        countries = self.cursor.fetchall()
        result = {value: country for country, value in countries if value}
        country = result[max(result.keys())]
        print("\tThe country with highest Covid-19 cases during week", date, "is", country)
        return country
    
    # Get the top 10 countries with lowest number of Covid-19 cases per 100 000 inhabitants
    def get_top_10_countries_with_lowest_covid_cases(self, date: str) -> list:
        date = self.convert_to_year_week(date)
        query = """
                SELECT country, (MIN(100 / NULLIF(cumulative_count, 0) * 100000))::float
                FROM covid_data
                WHERE cumulative_count > 0 AND year_week = %s
                GROUP BY cumulative_count, country
                """
        self.cursor.execute(query, (date,))
        countries = self.cursor.fetchall()
        top_10 = [country[0] for i, country in enumerate(countries) if i <= 9]
        for rank in top_10:
            print("\t", rank)
        return top_10

    # Get the top 10 countries with highest number of cases among the top 20 richest countries by GDP per capita
    def get_top_10_richest_countries_with_highest_num_of_cases(self, top_20: tuple[str, ...]) -> list:
        query = """
                SELECT c."Country", c."GDP ($ per capita)", covid_data.cumulative_count
                FROM countries_data c
                JOIN covid_data ON c."Country" = covid_data.country
                WHERE c."Country" IN ('Luxembourg', 'United States', 'Norway', 'Bermuda', 'Cayman Islands', 'San Marino', 'Switzerland', 'Denmark', 'Iceland', 'Austria', 'Canada', 'Ireland', 'Belgium', 'Australia', 'Hong Kong', 'Netherlands', 'Japan', 'Aruba', 'United Kingdom', 'Germany')
                ORDER BY covid_data.cumulative_count DESC
                LIMIT 10;
                """
        self.cursor.execute(query)
        
        # Attempt to automate the process, abandoned due to lack of time
        countries = []
        for country, rank in top_20:
            countries.append(country)
            
        top_10 = self.cursor.fetchall()
        for rank in top_10:
            print("\t", rank)
        return top_10

    # List all the regions with the number of cases per million of inhabitants
    def get_regions_with_the_number_of_cases_per_million(self, date: str) -> dict:
        date = self.convert_to_year_week(date)
        query = """
                SELECT DISTINCT country, (MAX(100 / NULLIF(cumulative_count, 0) * 1000000))::float
                FROM covid_data
                WHERE year_week = %s AND cumulative_count > 0
                GROUP BY country;
                """
        self.cursor.execute(query, (date,))
        regions = self.sort_fetch(self.cursor.fetchall())
        for country, cumulative_count in regions:
            print(f"\t{country}: {cumulative_count}")
        return regions

    # Query the data to find duplicated records
    def find_duplicates(self) -> list:
        query = """
                SELECT (covid_data.*)::text, count(*)
                FROM covid_data
                GROUP BY covid_data.*
                HAVING count(*) > 1
                """
        self.cursor.execute(query)
        duplicates = self.cursor.fetchall()
        print("\tThese duplicates in 'covid_data' have been identified:")
        for row in duplicates:
            print(row)
        return duplicates

class CountriesData(DatabaseConnect):
    def __init__(self):
        super().__init__()
        
    # Creates the countries_data table in PostgreSQL
    def create_countries_table(self) -> None:
        query = """
                CREATE TABLE IF NOT EXISTS countries_data (
                "Country" TEXT, 
                "Region" TEXT, 
                "Population" INTEGER, 
                "Area (sq. mi.)" TEXT,
                "Pop. Density (per sq. mi.)" TEXT, 
                "Coastline (coast/area ratio)" TEXT,
                "Net migration" TEXT, 
                "Infant mortality (per 1000 births)" TEXT,
                "GDP ($ per capita)" INTEGER, 
                "Literacy (%)" TEXT, 
                "Phones (per 1000)" TEXT,
                "Arable (%)" TEXT, 
                "Crops (%)" TEXT, 
                "Other (%)" TEXT, 
                "Climate" VARCHAR,
                "Birthrate" TEXT, 
                "Deathrate" TEXT, 
                "Agriculture" TEXT, 
                "Industry" TEXT,
                "Service" TEXT
                )
                """
        self.cursor.execute(query)
        print("\tTable 'countries_data' has been successfully created or already exists")

    
    # Reading the csv file
    def fetch_csv(self) -> None:     
        filename = "countries_of_the_world.csv"
        with open(filename, "r") as f:
            self.cursor.copy_expert(sql=f"COPY countries_data FROM STDIN CSV HEADER", file=f)
        print("\tTable 'countries_data' has been successfully populated")
 
    # Get top 20 richest countries by GDP per capita    
    def get_top_20_richest_countries(self) -> tuple[str, ...]:
        query = """
                SELECT DISTINCT countries_data."Country", "GDP ($ per capita)"
                FROM countries_data
                WHERE "GDP ($ per capita)" > 0 
                ORDER BY "GDP ($ per capita)" DESC
                LIMIT 20
                """
        self.cursor.execute(query)
        top_20 = self.cursor.fetchall()
        print("\tTop 20 countries with highest GDP per capita:")
        for country, GDP in top_20:
            print(f"\t\t{country} {GDP}")
        return tuple(top_20)
 
    # Display information on population density
    def display_information_on_population_density(self) -> dict:
        query = """
                SELECT MAX("Pop. Density (per sq. mi.)"), "Country"
                FROM countries_data
                GROUP BY "Country";
                """
        self.cursor.execute(query)
        data = self.cursor.fetchall()
        for country, density in data:
            print(f"\t{country}: {density}")
        return data

    # Query the data to find duplicated records
    def find_duplicates(self) -> list:
        query = """
                SELECT (countries_data.*)::text, count(*)
                FROM countries_data
                GROUP BY countries_data.*
                HAVING count(*) > 1
                """
        self.cursor.execute(query)
        duplicates = self.cursor.fetchall()
        print("\tThese duplicates in 'countries_data' have been identified:")
        for row in duplicates:
            print("\t", row)
        return duplicates

def main():
    covid_data = None
    countries_data = None
    
    try:
        # Creating class objects
        covid_data = CovidData()
        countries_data = CountriesData()
        
        print("\nEXERCISE 1:")
        # Creating respective tables in PostgreSQL database
        covid_data.create_covid_table()
        countries_data.create_countries_table()
        
        # Fetch JSON file with Covid data
        data = covid_data.fetch_json()
        
        print("\nEXERCISE 2:")
        # Load JSON file with Covid data into PostreSQL database and create a pipeline
        covid_data.run_data_pipeline(data)
        
        # Load CSV file with countries data into PostgreSQL database
        countries_data.fetch_csv()
        
        print("\nEXERCISE 3:")
        # Create view with the latest number of cases spanning past 2 weeks
        covid_data.create_view_cases_last_fortnight()

        print("\nEXERCISE 4:")
        date = "31/07/2020"
        
        print("Exercise 4.1:")
        # Get country with the highest number of Covid-19 cases per 100 000 inhabitants
        covid_data.get_country_with_highest_covid_cases(date)

        print("\nExercise 4.2:\nTop 10 countries with the lowest Covid-19 cases in", date, ":")
        # Get the top 10 countries with lowest number of Covid-19 cases per 100 000 inhabitants
        covid_data.get_top_10_countries_with_lowest_covid_cases(date)

        print("\nExercise 4.3:\nTop 10 countries with the highest number of Covid-19 cases among the top 20 richest countries:")
        # Get the top 10 countries with highest number of cases among the top 20 richest countries by GDP per capita
        top_20 = countries_data.get_top_20_richest_countries()
        covid_data.get_top_10_richest_countries_with_highest_num_of_cases(top_20)

        print("\nExercise 4.4:\nRegions with the number of cases per million of inhabitants:")
        # List all the regions with the number of cases per million of inhabitants
        regions = covid_data.get_regions_with_the_number_of_cases_per_million(date)

        print("\nInformation on population density:")
        # Display information on population density
        countries_data.display_information_on_population_density()
        
        print("\nExercise 4.5:")
        # Query the data to find duplicated records
        covid_data.find_duplicates()
        countries_data.find_duplicates()
        
    except (Exception, psycopg2.DatabaseError) as Error:
        print(Error)

    finally:
        # Close connections
        covid_data.close_connection()
        countries_data.close_connection()
        
if __name__ == '__main__':
    main()